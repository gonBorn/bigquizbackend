package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Repository.GoodsRepository;
import com.twuc.webApp.Repository.OrderRepository;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import sun.jvm.hotspot.runtime.ObjectMonitor;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@Transactional
public class OrderControllerTest extends ApiTestBase{

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private EntityManager manager;

  @Autowired
  private GoodsRepository goodsRepository;

  @Autowired
  private OrderRepository orderRepository;

  @BeforeEach
  void setUp() {
    Goods goods = new Goods("可乐", new BigDecimal(2), "瓶", "ddadasdas");
    Goods save = goodsRepository.save(goods);
  }

  @Test
  void create_order_test() {
    Goods goods = new Goods("雪碧", new BigDecimal(2), "瓶", "ddadasdas");
    goodsRepository.save(goods);
    Order order = new Order(goods,2);
    orderRepository.save(order);
    manager.flush();
    manager.clear();
  }

  @Test
  void update_order_test() throws Exception {
    Optional<Order> byId = orderRepository.findById(1L);
    assertFalse(byId.isPresent());
    mockMvc.perform(patch("/mall/orders/update/1"));
    Optional<Order> byId1 = orderRepository.findById(1L);
    assertTrue(byId1.isPresent());
  }

  @Test
  void should_return_null_when_delete_order_test() throws Exception {
    Goods goods = new Goods("雪碧", new BigDecimal(2), "瓶", "ddadasdas");
    goodsRepository.save(goods);
    Order order = new Order(goods,2);
    orderRepository.save(order);
    assertNotNull(orderRepository.getOne(1L));
    mockMvc.perform(delete("/mall/orders/delete/1"));
    Optional<Order> byId = orderRepository.findById(1L);
    assertFalse(byId.isPresent());
  }
}
