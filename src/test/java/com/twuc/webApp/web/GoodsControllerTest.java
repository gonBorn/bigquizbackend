package com.twuc.webApp.web;

import com.twuc.webApp.Repository.GoodsRepository;
import com.twuc.webApp.Repository.OrderRepository;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class GoodsControllerTest extends ApiTestBase{
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private EntityManager manager;

  @Autowired
  private GoodsRepository goodsRepository;

  @BeforeEach
  void setUp() {
    Goods goods = new Goods("可乐", new BigDecimal(2), "瓶", "ddadasdas");
    Goods save = goodsRepository.save(goods);
  }

  @Test
  void get_goods_test() throws Exception {
    mockMvc.perform(get("/mall/goods").contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isArray());
  }

  @Test
  void create_new_goods_test() {
    Goods goods = new Goods("aa", new BigDecimal(2.00), "a", "ddadasdas");
    Goods save = goodsRepository.save(goods);
    manager.flush();
    manager.clear();
    Optional<Goods> get = goodsRepository.findById(save.getId());
    System.out.println(save.toString());
    System.out.println(get.get().toString());
    assertEquals(save,get.get());
  }

}
