CREATE TABLE IF NOT EXISTS goods(
    id BIGINT AUTO_INCREMENT,
    name VARCHAR(10) NOT NULL,
    price DECIMAL(5,2) NOT NULL,
    unit varchar(1) NOT NULL,
    url varchar(256) NOT NULL,
    PRIMARY KEY (id)
);