CREATE TABLE IF NOT EXISTS orders (
    id BIGINT AUTO_INCREMENT ,
    goods_id BIGINT NOT NULL ,
    num INT NOT NULL ,
    PRIMARY KEY (id)
);

ALTER TABLE orders
ADD FOREIGN KEY fk_orders_goods(goods_id)
REFERENCES goods (id);
