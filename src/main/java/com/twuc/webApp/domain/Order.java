package com.twuc.webApp.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orders")
public class Order {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne
  private Goods goods;

  @Column(nullable = false)
  private Integer num;

  public Order() {
  }

  public Order(Goods goods, Integer num) {
    this.goods = goods;
    this.num = num;
  }



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Goods getGoods() {
    return goods;
  }

  public void setGoods(Goods goods) {
    this.goods = goods;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Order order = (Order) o;
    return Objects.equals(id, order.id) &&
      Objects.equals(goods, order.goods) &&
      Objects.equals(num, order.num);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, goods, num);
  }

  @Override
  public String toString() {
    return "Order{" +
      "id=" + id +
      ", goods=" + goods +
      ", num=" + num +
      '}';
  }
}
