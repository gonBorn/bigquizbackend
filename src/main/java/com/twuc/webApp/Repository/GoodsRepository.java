package com.twuc.webApp.Repository;

import com.twuc.webApp.domain.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsRepository extends JpaRepository<Goods, Long> {
  Goods findByName(String name);
}
