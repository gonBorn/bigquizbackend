package com.twuc.webApp.Repository;

import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
  Order findFirstByGoods(Goods goods);
}
