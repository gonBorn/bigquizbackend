package com.twuc.webApp.contracts;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreateGoodsRequest {
  @NotNull
  private String name;
  @DecimalMin(value = "0.0")
  private BigDecimal price;
  @NotNull
  @Size(min = 1,max = 2)
  private String unit;
  @NotNull
  private String url;

  public CreateGoodsRequest() {
  }

  public CreateGoodsRequest(@NotNull String name, @DecimalMin(value = "0.0") BigDecimal price, @NotNull @Size(min = 1, max = 2) String unit, @NotNull String url) {
    this.name = name;
    this.price = price;
    this.unit = unit;
    this.url = url;
  }

  public String getName() {
    return name;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public String getUnit() {
    return unit;
  }

  public String getUrl() {
    return url;
  }
}
