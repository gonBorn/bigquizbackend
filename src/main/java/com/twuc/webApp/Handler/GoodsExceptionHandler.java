package com.twuc.webApp.Handler;

import com.twuc.webApp.Controller.GoodsController;
import com.twuc.webApp.Exception.GoodsAlreadyExistException;
import com.twuc.webApp.Service.GoodsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(value = "GoodsController.class")
public class GoodsExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(GoodsAlreadyExistException.class)
  public ResponseEntity handleGoodsAlresdyExists(RuntimeException e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
      .body("商品名称已存在,请输入新的商品名");
  }
}
