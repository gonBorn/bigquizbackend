package com.twuc.webApp.Controller;

import com.twuc.webApp.Exception.GoodsAlreadyExistException;
import com.twuc.webApp.Service.GoodsService;
import com.twuc.webApp.contracts.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/mall/goods")
@CrossOrigin(origins = "*")
public class GoodsController {

  private final GoodsService goodsService;

  public GoodsController(GoodsService goodsService) {
    this.goodsService = goodsService;
  }

  @GetMapping
  public ResponseEntity<List<Goods>> getAllGoods () {
    return ResponseEntity.status(HttpStatus.OK)
      .body(goodsService.getAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Goods> getGoodsById(@PathVariable Long id) {
    return ResponseEntity.status(HttpStatus.OK)
      .body(goodsService.getOneGoods(id));
  }

  @PostMapping("/create")
  public ResponseEntity createGoods(@RequestBody @Valid CreateGoodsRequest request) throws GoodsAlreadyExistException {
    goodsService.createGoods(request);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

}
