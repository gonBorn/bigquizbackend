package com.twuc.webApp.Controller;

import com.twuc.webApp.Service.OrderService;
import com.twuc.webApp.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/mall/orders")
@RestController
@CrossOrigin(origins = "*")
public class OrdersController {

  @Autowired
  private OrderService orderService;

  @GetMapping
  public ResponseEntity<List<Order>> getOrders() {
    List<Order> orders = orderService.getOrders();
    return ResponseEntity.status(HttpStatus.OK)
      .body(orders);
  }

  @PatchMapping("/update/{goods_id}")
  public ResponseEntity updateOrder(@PathVariable Long goods_id) {
    orderService.updateOrder(goods_id);
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @DeleteMapping("/delete/{order_id}")
  public ResponseEntity deleteOrder(@PathVariable Long order_id) {
    orderService.deleteOrder(order_id);
    return ResponseEntity.status(HttpStatus.OK).build();
  }

}
