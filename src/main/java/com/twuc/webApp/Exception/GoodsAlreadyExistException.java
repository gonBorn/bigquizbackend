package com.twuc.webApp.Exception;

public class GoodsAlreadyExistException extends Throwable {
  public GoodsAlreadyExistException(String message) {
    super(message);
  }
}
