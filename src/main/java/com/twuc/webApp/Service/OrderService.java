package com.twuc.webApp.Service;

import com.twuc.webApp.Repository.GoodsRepository;
import com.twuc.webApp.Repository.OrderRepository;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

  @Autowired
  private OrderRepository orderRepository;
  @Autowired
  private GoodsRepository goodsRepository;

  public List<Order> getOrders() {
    return orderRepository.findAll();
  }

  public void updateOrder(Long goods_id) {
    Goods goods = goodsRepository.findById(goods_id).orElseThrow(RuntimeException::new);
    Order order = orderRepository.findFirstByGoods(goods);
    if (null == order) {
      orderRepository.save(new Order(goods,1));
    } else {
      order.setNum(order.getNum()+1);
      orderRepository.save(order);
    }
  }

  public void deleteOrder(Long order_id) {
    Order order = orderRepository.findById(order_id).get();
    orderRepository.delete(order);
  }
}
