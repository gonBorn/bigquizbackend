package com.twuc.webApp.Service;

import com.twuc.webApp.Exception.GoodsAlreadyExistException;
import com.twuc.webApp.Repository.GoodsRepository;
import com.twuc.webApp.contracts.CreateGoodsRequest;
import com.twuc.webApp.domain.Goods;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GoodsService {

  @Autowired
  private GoodsRepository goodsRepository;

  public Goods getOneGoods(Long id) {
    return goodsRepository.findById(id).get();
  }

  public List<Goods> getAll() {
    return goodsRepository.findAll();
  }

  public void createGoods(CreateGoodsRequest request) throws GoodsAlreadyExistException {
    if (null != goodsRepository.findByName(request.getName())) {
      throw new GoodsAlreadyExistException("this goods already exist.");
    }
    else {
      Goods newGoods = new Goods();
      BeanUtils.copyProperties(request,newGoods);
      goodsRepository.save(newGoods);
    }

  }
}
